"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
import os
import pickle
import time

import pandas as pd

from xpresso.ai.core.commons.utils.xpr_exceptions import XprExceptions
from xpresso.ai.core.data.inference.abstract_inference_service import AbstractInferenceService
from xpresso.ai.core.commons.utils.constants import *
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Naveen Sinha"

logging = XprLogger("retail_sales_project")


class XGBoostInference(AbstractInferenceService):

    def __init__(self):
        super().__init__()
        self.feature_cols = ['CompetitionDistance', 'Promo', 'Promo2',
                             'NewAssortment', 'NewStoreType']

    def load(self):
        """
        Fetches data from a data versioning repo using the args provided
        (commit id, repo name, etc.)
        Returns: model info

        """
        try:
            repo_name = os.environ['REPO_NAME']
            commit_id = os.environ['COMMIT_ID']
            dv_path = os.environ['DATA_VERSIONING_PATH']

            response_project = self.api_utils.get_project_info(
                {PROJECT_NAME_IN_PROJECTS: repo_name}
            )
            project_token = response_project[PROJECT_TOKEN_IN_PROJECTS]
            controller_factory = VersionControllerFactory(**{
                DV_PROJECT_TOKEN: project_token
            })
            version_controller = controller_factory.get_version_controller()

            commit_path = version_controller.pull_dataset(repo_name=repo_name,
                                                          xpresso_commit_id=commit_id,
                                                          path=dv_path,
                                                          output_type=
                                                          OUTPUT_TYPE_FILES)
            model_path = commit_path + dv_path
            print("Waiting 10 seconds before starting the application...")
            time.sleep(10)
            self.load_model(model_path)
        except KeyError as e:
            faulty_key = e.args[0]
            print(f"Failed to fetch '{faulty_key}'.")
            raise XprExceptions("Failed to fetch the model.Exiting.")
        except XprExceptions as e:
            print(f"Failed to fetch the model from data versioning repository."
                  f"\nDetails: {e.message}")
            raise XprExceptions("Exiting failed to fetch the model")

    def load_model(self, model_path):
        with open(os.path.join(model_path, "xgboost.pkl"), "rb") as model_fs:
            self.model = pickle.load(model_fs)
            logging.info(self.model)

    def transform_input(self, input_request):
        feature = pd.DataFrame()
        if isinstance(input_request, list):
            feature = pd.DataFrame([input_request], columns=self.feature_cols)
        elif isinstance(input_request, dict):
            try:
                data_to_list = [input_request[col] for col in self.feature_cols]
                feature = pd.DataFrame(data=[data_to_list],
                                       columns=self.feature_cols)
            except (ValueError, TypeError, IndexError):
                raise XprExceptions(message="Invalid data input")
        else:
            raise XprExceptions(message="Invalid data input")
        return feature

    def transform_output(self, output_response):
        return output_response

    def predict(self, input_request):
        logging.info(self.model)
        result = self.model.predict(input_request)
        logging.info(list(result))
        return result.tolist()

    def report_inference_status(self, service_info, status):
        pass


if __name__ == "__main__":
    pred = XGBoostInference()
    # To run locally. Use load_model instead
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=8888)
