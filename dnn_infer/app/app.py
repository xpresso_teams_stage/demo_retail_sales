"""
This is a inference service for dense neural network model
"""

import os
import pandas as pd
import time
import tensorflow as tf

from xpresso.ai.core.commons.utils.xpr_exceptions import XprExceptions
from xpresso.ai.core.commons.utils.constants import *
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger


__author__ = "Naveen Sinha"

logging = XprLogger("retail_sales_project")


class DNNInference(AbstractInferenceService):
    """ This is the main class for inference service. It implements predict and
    supporting functionality. REST API is automatically gengerated by the
    Abstract inference service """
    def __init__(self):
        super().__init__()
        categorical_cols = ['Store', 'DayOfWeek', 'Promo', 'StateHoliday',
                            'SchoolHoliday', 'StoreType', 'Assortment',
                            'Promo2', 'Day', 'Month', 'Year', 'isCompetition']

        numeric_cols = ['CompetitionDistance']
        self.all_cols = categorical_cols + numeric_cols

    def load(self):
        """
        Fetches data from a data versioning repo using the args provided
        (commit id, repo name, etc.)
        Returns: model info

        """
        try:
            repo_name = os.environ['REPO_NAME']
            commit_id = os.environ['COMMIT_ID']
            dv_path = os.environ['DATA_VERSIONING_PATH']

            response_project = self.api_utils.get_project_info(
                {PROJECT_NAME_IN_PROJECTS: repo_name}
            )
            project_token = response_project[PROJECT_TOKEN_IN_PROJECTS]
            controller_factory = VersionControllerFactory(**{
                DV_PROJECT_TOKEN: project_token
            })
            version_controller = controller_factory.get_version_controller()

            commit_path = version_controller.pull_dataset(repo_name=repo_name,
                                                          xpresso_commit_id=commit_id,
                                                          path=dv_path,
                                                          output_type=
                                                          OUTPUT_TYPE_FILES)
            model_path = commit_path + dv_path
            print("Waiting 10 seconds before starting the application...")
            time.sleep(10)
            self.load_model(model_path)
        except KeyError as e:
            faulty_key = e.args[0]
            print(f"Failed to fetch '{faulty_key}'.")
            raise XprExceptions("Failed to fetch the model.Exiting.")
        except XprExceptions as e:
            print(f"Failed to fetch the model from data versioning repository."
                  f"\nDetails: {e.message}")
            raise XprExceptions("Exiting failed to fetch the model")

    def exp_rmspe(self, y_true, y_pred):
        """Competition evaluation metric, expects logarthmic inputs."""
        pct = tf.square((tf.exp(y_true) - tf.exp(y_pred)) / tf.exp(y_true))
        # Compute mean excluding stores with zero denominator.
        x = tf.reduce_sum(tf.where(y_true > 0.001, pct, tf.zeros_like(pct)))
        y = tf.reduce_sum(
            tf.where(y_true > 0.001, tf.ones_like(pct), tf.zeros_like(pct)))
        return tf.sqrt(x / y)

    def act_sigmoid_scaled(self, x):
        """Sigmoid scaled to logarithm of maximum sales scaled by 20%."""
        return tf.nn.sigmoid(x) * tf.compat.v1.log(42000.00) * 1.2

    def load_model(self, model_path):
        """ Loads the model into memory """
        CUSTOM_OBJECTS = {'exp_rmspe': self.exp_rmspe,
                          'act_sigmoid_scaled': self.act_sigmoid_scaled}
        self.model = tf.keras.models.load_model(os.path.join(model_path, "saved_model.h5"),
                                                custom_objects=CUSTOM_OBJECTS)

    def transform_input(self, input_request):
        """ Receives the json input data from the Rest API. This is converted
        into a dataframe object with the provided input """
        data_to_list = [input_request[col] for col in self.all_cols]
        logging.info(data_to_list)
        feature = pd.DataFrame(data=[data_to_list],
                               columns=self.all_cols)
        logging.info([feature[col].values for col in self.all_cols])
        return feature

    def transform_output(self, output_response):
        """ This prepares the response. Output from predicts is converted into
        a response json serializable object. No need to change anything
        here """
        return output_response

    def predict(self, input_request):
        """ Main predict function for predicting the sales """
        tf_vector = [tf.convert_to_tensor(input_request[col].values) for col in
                     self.all_cols]
        value = self.model.predict(tf_vector)
        logging.info(value)
        logging.info(type(value[0]))
        return value[0].tolist()

    def report_inference_status(self, service_info, status):
        " Not in user right now "
        pass


if __name__ == "__main__":
    """ Main file to start the execution 
    It takes run_id parameters from command line"""
    pred = DNNInference()
    # To run locally. Use load_model instead
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=8889)
